package main.runner;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

import com.jacob.com.LibraryLoader;

import main.utils.Constants;

public class TestRunner {

	public static void main(String[] args) {
		File jacob = new File("lib", "jacob-1.18-x64.dll"); //path to the jacob dll
	    System.setProperty(LibraryLoader.JACOB_DLL_PATH, jacob.getAbsolutePath());
		
		List<String> file = new ArrayList<String>();
	    file.add(Constants.TESTDATA_PATH + "\\testng.xml");
	    TestNG testNG = new TestNG();
	    testNG.setUseDefaultListeners(false);
	    testNG.setTestSuites(file);
	    testNG.run();
	}

}