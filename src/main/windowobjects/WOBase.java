package main.windowobjects;

import main.utils.CustomLogger;

public class WOBase {

public static void logInfo(String logMes){
	CustomLogger.logInfo(logMes);
}

public static void logError(String logMes){
	CustomLogger.logError(logMes);
}

}
