package main.tests;

import org.testng.ITest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import autoitx4java.AutoItX;
import main.utils.Constants;
import main.utils.CustomLogger;
import main.utils.ExcelUtils;
import main.windowobjects.Arved;
import main.windowobjects.Põhiaken;
import main.windowobjects.SisenemineNäidisettevõte;
import main.windowobjects.TereTulemast;
import main.windowobjects.UusArve;
import main.windowobjects.VaataArve;

public class TestBase implements ITest{
	
	public static void logInfo(String logMes)	{
		CustomLogger.logInfo(logMes);
	}
	
	public static void logError(String logMes)	{
		CustomLogger.logError(logMes);
	}
	
    protected static AutoItX x = new AutoItX();
    protected static Põhiaken põhiaken = new Põhiaken();
    protected static UusArve uusArve = new UusArve();
    protected static VaataArve vaataArve = new VaataArve();
    protected static Arved arved = new Arved();
    protected static TereTulemast tereTulemast = new TereTulemast();
    protected static SisenemineNäidisettevõte sisenemineNäidisettevõte = new SisenemineNäidisettevõte();
    
    public void clickRelative(String title, int[] coords){
    	x.mouseClick("primary", x.winGetPosX(title) + coords[0], x.winGetPosY(title) + coords[1], 1, 0);
    }
    
    public void clickRelativeRight(String title, int[] coords){
    	x.mouseClick("primary", x.winGetPosX(title) + x.winGetClientSizeWidth(title) - coords[0], x.winGetPosY(title) + coords[1], 1, 0);
    }
    
    public String getSelectedText(){
    	String s = x.clipGet();
    	x.send("^c", false);
    	String target = x.clipGet();
    	x.clipPut(s);
    	return target;
    }
	
	protected static String testName = "Test Name Initialized";
	
	public String getThisTestName(){
		return this.getClass()
			.getSimpleName()
			.replace("Test", "")
			.replaceAll("(\\p{Ll})(\\p{Lu})","$1 $2");
	}
	
	@DataProvider(name = "TestData")
	public Object[][] TestData() throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(Constants.TESTDATA_PATH + "\\" + Constants.TESTDATA_FILE, getThisTestName());
        return (testObjArray);
	}
	
	@Override
	public String getTestName() {
		return testName;
	}

	@BeforeSuite
	public void setUpSuite() {
		CustomLogger.setUpExtentReport();
	    }
	
	@BeforeMethod
	public void setUpMethod() {
		testName = getThisTestName().concat(" ");
		
		x.run("C:\\Program Files (x86)\\HansaWorld\\Standard Books\\Standard Books.exe", "C:\\Program Files (x86)\\HansaWorld\\Standard Books\\");
		
		x.winActivate(tereTulemast.title);
		x.winWaitActive(tereTulemast.title);
		x.controlClick(tereTulemast.title, "", tereTulemast.näidisettevõteBtn);
		
		x.winWaitActive(sisenemineNäidisettevõte.title);
		clickRelative(sisenemineNäidisettevõte.title, sisenemineNäidisettevõte.okBtnCoord);
	}
	
	@AfterMethod
	public void teadDownMethod() {
		TestBase.testName = "Test name reset by @AfterMethod";
		
		x.winActivate(põhiaken.title);
		x.winWaitActive(põhiaken.title);
		x.send("^q", false);
	}

	@AfterSuite
	public void tearDownSuite()	{
		CustomLogger.finishExtentReport();
	}
	
}
