package main.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestArvedAddEntry extends TestBase{

	@Test(dataProvider = "TestData")
	public void test(String klient)	{
		
		x.winActivate(p�hiaken.title);
		x.winWaitActive(p�hiaken.title);
		x.controlClick(p�hiaken.title, "", p�hiaken.arvedBtn);
		
		x.winWaitActive(arved.title);
		x.controlClick(arved.title, "", arved.uusBtn);
		
		x.winWaitActive(uusArve.title);
		clickRelative(uusArve.title, uusArve.nrFieldCoord);
		x.sleep(200);
		String uusArveNr = getSelectedText();
		x.sleep(200);
		clickRelative(uusArve.title, uusArve.klientFieldCoord);x.sleep(200);
		x.send(klient);x.sleep(200);
		x.controlClick(uusArve.title, "",uusArve.salvestaBtn);
		x.sleep(200);
		x.winClose(uusArve.title);
		
		x.winWaitActive(arved.title);
		clickRelativeRight(arved.title, arved.searchBoxPos);x.sleep(200);
		x.send(uusArveNr + "{ENTER}" + "{ENTER}", false);
		
		x.winWaitActive(vaataArve.title);
		x.sleep(200);
		clickRelative(vaataArve.title, vaataArve.nrFieldCoord);
		clickRelative(vaataArve.title, vaataArve.nrFieldCoord); //doubleclick
		x.sleep(200);
		Assert.assertTrue(getSelectedText().contains(uusArveNr));
		x.winClose(vaataArve.title);
		
		x.winClose(arved.title);
				
	}
	
}
