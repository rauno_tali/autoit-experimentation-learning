package main.utils;

import java.io.FileInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
	private static XSSFSheet ExcelWSheet; 
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	
	private static final Logger log = LogManager.getLogger(ExcelUtils.class);
	
	public static void setExcelFile(String Path,String SheetName) throws Exception {
		try {
			FileInputStream ExcelFile = new FileInputStream(Path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
		} 
		catch (Exception e)	{
			log.error("Unable to access Excel file!\n" + e.getMessage());
			System.exit(1);
		}
	}
	
	public static Object getCellData(int RowNum, int ColNum) throws Exception	{
		int type;
		Object result = null;
		try
		{
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			type = Cell.getCellType();
			
			switch(type) {
		    case 0://numeric value in excel
		        result = (long) Cell.getNumericCellValue();
		        break;
		    case 1: //string value in excel
		        result = Cell.getStringCellValue();
		        break;
		    case 2: //boolean value in excel
		        result = Cell.getBooleanCellValue();
		        break;
		    case 3: //blank value
		    	result = null;
		    	break;
		    default:
		    	throw new Exception("There is no support for this type of cell");
			}
		}
		catch (Exception e)
		{
			log.error("Unable to read data from cell in Excel file! Program will close.\n" + e.getMessage());
			System.exit(2);
		}
		return result;
	}
	
	public static Object[][] getTableArray(String Path, String SheetName) throws Exception {
		Object[][] tabArray = null;
		try
		{
			setExcelFile(Path, SheetName);
			int startRow = 1;
			int startCol = 1;
			int totalRows = ExcelWSheet.getLastRowNum();
			//System.out.println(ExcelWSheet.getRow(0).getLastCellNum()-1);
			int totalCols = ExcelWSheet.getRow(0).getLastCellNum()-1;
	
			tabArray = new Object[totalRows][totalCols];
	
			for (int i=startRow, ci = 0; i <= totalRows; i++, ci++)
				for (int j=startCol, cj = 0; j <= totalCols ;j++, cj++)
					tabArray[ci][cj] = getCellData(i,j);
		}
		
		catch (Exception e){
			log.error("Unable to get data from Excel file!\n" + CustomLogger.convertStackTrace(e));
			System.exit(3);
		}

		return(tabArray);
	}

}
