package calcTest;

import java.io.File;

import org.testng.Assert;

import com.jacob.com.LibraryLoader;

import autoitx4java.AutoItX;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file = new File("lib", "jacob-1.18-x64.dll"); //path to the jacob dll
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

        AutoItX x = new AutoItX();
        
        String calc = "Calculator";
        String nr[] = {"3","4"};
        String target = "7";
        
        System.out.println("Running calc.exe");
        x.run("calc.exe");
        
        System.out.println("Running test on window '" + calc + "'");
        x.winWaitActive(calc);
        Assert.assertTrue(x.winExists(calc), "Window with name '" + calc + "' exists");
        x.send(nr[0]);
        x.controlClick(calc, nr[0], "[ID:93]");
        x.send(nr[1]);
        x.send("=");
        System.out.println("Confirming results of 3 + 4 = 7");
        Assert.assertTrue(x.winExists(calc, target), "Window with name '" + calc + "' and text '" + target + "' exists");
        
        System.out.println("Closing window '" + calc + "'");
        x.winClose(calc);
        Assert.assertFalse(x.winExists(calc), "Window with name '" + calc + "' exists");
        
        System.out.println("Running false assert");
        Assert.assertTrue(x.winExists(calc), "Window with name '" + calc + "' exists");
        
	}

}
